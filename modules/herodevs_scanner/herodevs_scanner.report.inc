<?php

/**
 * @file
 * Report building utilities.
 */

use Drupal\herodevs_scanner\NES\NesDependencyTable;

/**
 * Page callback to build up a full report.
 */
function herodevs_scanner_reports_page() {

  drupal_add_library('system', 'jquery');
  drupal_add_js(
    drupal_get_path('module', 'herodevs_scanner')
      . '/js/nes-dep-report.js',
    ['type' => 'file']
  );

  // We're going to be borrowing heavily from the update module
  module_load_include('inc', 'update', 'update.report');
  update_refresh();
  $available = update_get_available(TRUE);

  if ($available) {
    module_load_include('inc', 'update', 'update.compare');
    $data = update_calculate_project_data($available);
    $data = herodevs_scanner_calculate_report_data($data);

    $markup = theme('herodevs_scanner_report', array('data' => $data));
    $build['report']['#markup'] = $markup;
    $build['form'] = drupal_get_form('herodevs_scanner_report_form', $data);

    return $build;
  }

  return theme('update_report', array('data' => _update_no_data()));
}

/**
 * STEP 1: get the module catalog and schedule projects to check their status
 *
 * Page callback to rebuild the HeroDevs scanner report.
 */
function herodevs_scanner_reports_rebuild() {
  $s = new \Drupal\herodevs_scanner\NES\NesDependencyReportScheduler();
  $s->schedule_batch(TRUE);
  batch_process('admin/reports/herodevs');
  drupal_goto('admin/reports/herodevs');
}

/**
 * Batch callback to build the HeroDevs scanner report.
 */
function herodevs_scanner_build_report_batch($id, $project, $updates, &$context) {
  if (!isset($context['results']['report'])) {
    $context['results']['report'] = array();
  }


  // TODO: research how to dynamically add steps using $batch
  // $batch = &batch_get();
  // \NES\Log::log('Got the batch for context ' . $id, $batch);
  // $context['sandbox']['#operations'][] = array('herodevs_scanner_build_report_batch_dynamic', array($id, $project));

  // $result = \Drupal\herodevs_scanner\NES\NesDependencyTable::processReport($id, $project);
  $report = new \Drupal\herodevs_scanner\NES\NesDependencyReportBatch($id, $project, $updates);
  $result = $report->process();
  $context['results']['report'][$id] = $result;
  $context['message'] = t('Finished processing: @name', array('@name' => $id));
}

/**
 * Completion callback for the report batch.
 */
function herodevs_scanner_build_report_batch_finished($success, $results, $operations) {
  if ($success) {
    // Store them.
    cache_set('herodevs:full-report', $results['report'] ?? []);
    variable_set('herodevs_scanner_last_report', time());
  } else {
    // TODO: what to do upon failure?
  }
}
