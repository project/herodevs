<?php

/**
 * @file
 *   The settings forms for the HeroDevs scanner.
 */

/**
 * The settings form for the HeroDevs scanner.
 */
function herodevs_scanner_settings_form($form, $form_state) {
  $form = array();

  $settings['herodevs_scanner_settings_organization_name'] = array(
    '#type' => 'textfield',
    '#title' => 'Organization name (optional)',
    '#description' => 'Your organization name.',
    '#default_value' => variable_get('herodevs_scanner_settings_organization_name', ''),
    '#weight' => 10,
  );

  $settings['herodevs_scanner_settings_email'] = array(
    '#type' => 'textfield',
    '#title' => 'E-mail (optional)',
    '#description' => 'The e-mail address that you want to provide to HeroDevs to contact you.',
    '#default_value' => variable_get('herodevs_scanner_settings_email', ''),
    '#weight' => 20,
  );

  $settings['herodevs_scanner_settings_notes'] = array(
    '#type' => 'textarea',
    '#title' => 'Notes',
    '#description' => 'Notes that you want HeroDevs to know.',
    '#default_value' => variable_get('herodevs_scanner_settings_notes', ''),
    '#weight' => 30,
  );

  $settings['herodevs_scanner_settings_url'] = array(
    '#type' => 'textfield',
    '#title' => 'API Endpoint URL',
    '#description' => 'The URL of the HeroDevs API endpoint.',
    '#default_value' => variable_get('herodevs_scanner_settings_url', 'https://api.nes.herodevs.com/'),
    '#weight' => 35,
  );

  $settings['herodevs_scanner_settings_access_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Access token (if you have one)',
    '#description' => 'Access token that you got from HeroDevs.',
    '#default_value' => variable_get('herodevs_scanner_settings_access_token', ''),
    '#weight' => 40,
  );

  $settings['herodevs_scanner_settings_use_cron'] = array(
    '#type' => 'checkbox',
    '#title' => 'Refresh HeroDevs data during cron',
    '#description' => 'Opt into automatic refresh of HeroDevs data during cron runs.',
    '#default_value' => variable_get('herodevs_scanner_settings_use_cron', FALSE),
    '#weight' => 50,
  );

  $settings['message_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Message settings'),
    '#weight' => 60,
  );

  $settings['message_fieldset']['herodevs_scanner_settings_position'] = array(
    '#type' => 'select',
    '#title' => t('Position'),
    '#description' => t('Position of the message bar.'),
    '#options' => array(
      'top' => t('Top'),
      'bottom' => t('Bottom'),
    ),
    '#default_value' => variable_get('herodevs_scanner_settings_position', 'bottom'),
    '#weight' => 30,
  );

  $form['herodevs_scanner_settings'] = $settings;

  $form['#validate'][] = 'herodevs_scanner_settings_form_validate';

  return system_settings_form($form);
}

function herodevs_scanner_settings_form_validate($form, &$form_state) {
  $email = $form_state['values']['herodevs_scanner_settings_email'] ?? '';

  if (!empty($email) && !valid_email_address($email)) {
    form_set_error('herodevs_scanner_settings_email', t('You must enter a valid e-mail address.'));
  }

  if (!empty($form_state['values']['herodevs_scanner_settings_url'])) {
    $url = $form_state['values']['herodevs_scanner_settings_url'];
    if (!valid_url($url)) {
      form_set_error('herodevs_scanner_settings_url', t('You must enter a valid URL.'));
    }
  }
}
