; (function ($) {
  $(document).ready(function () {
    $('.hd-project--status').each((i, $e) => {
      const $el = $($e)
      const el = {
        status: $el,
        project: $el.closest('.hd-project--root')
      }

      // const fqns = el.project.attr('data-fqns')
      // console.log(fqns, el.status)

      el.status.bind('click', function () {
        el.project.toggleClass('expanded')
      })
    })
  })
})(jQuery)