<?php

namespace Drupal\herodevs_scanner\NES;

class NesDependencyReportScheduler {

  // function herodevs_scanner_calculate_project_data($projects, $force = FALSE, $redirect = NULL)
  function schedule_batch() {
    // we make an initial (blocking) call to get the report catalog
    $client = new \Drupal\herodevs_scanner\NES\NesClient(variable_get('herodevs_scanner_settings_url') . 'graphql');
    $insights = $client->getPackageAdvisories(['drupal_7']);

    // next, we create an oracle, because it's waaaay nicer to work with...
    $oracle = new \Drupal\herodevs_scanner\NES\NesDrupalOracle($insights);
    $mapping = $oracle->getMapping();
    // \NES\Log::log('Insights received!', $mapping);

    if (defined('NES_DEBUG_CORE_ONLY') == true) {
      $mapping = [$mapping['drupal/core']];
    }



    $mods = update_get_projects();
    $mods = update_calculate_project_data($mods);

    // we have to override the current instance version to get the CONSTANT
    if (array_key_exists('drupal', $mods)) {
      $mods['drupal']['existing_version'] = VERSION;
    }
    // \NES\Log::log('here with mods', $mods['drupal']);
    // $herodevs_projects = $mods['drupal_projects'];

    // \NES\Log::log('look at mods', $mods);


    $enabled_mods = array_map(function ($id) {
      $fqns = \Drupal\herodevs_scanner\NES\NesDrupalOracle::getModuleId($id);
      return (object) [
        'id' => $id,
        'fqns' => $fqns ? $fqns : '',
      ];
    }, array_keys(module_list()));
    $enabled_mods = array_combine(array_column($enabled_mods, 'id'), $enabled_mods);
    // array_unshift($enabled_mods, 'drupal/core');

    // TODO START HERE
    $operations = array();
    foreach ($mapping as $id => $project) {
      // Remove the drupal/ from the FQNS.
      $id = str_replace('drupal/', '', $id);
      if (!array_key_exists($id, $enabled_mods)) {
        continue;
      }

      $update_details = $mods[$id];
      $operations[] = array(
        'herodevs_scanner_build_report_batch',
        array($project->fqns, $project, $update_details),
      );
    }

    // TODO: remove this debug data
    // // $id = array_key_first($projects);
    $id = 'drupal/core';
    $project = isset($mapping[$id]) ? $mapping[$id] : NULL;
    $update_details = $mods['drupal'];
    if ($project) {
      $operations[] = [
        'herodevs_scanner_build_report_batch',
        [$project->fqns, $project, $update_details],
      ];
    }


    // \NES\Log::log('batch ops', $operations);


    $batch = array(
      'operations' => $operations,
      'finished' => 'herodevs_scanner_build_report_batch_finished',
      'file' => drupal_get_path('module', 'herodevs_scanner') . '/herodevs_scanner.report.inc',
      'title' => t('Building report'),
    );

    // \NES\Log::log('Batch prepared', $batch);

    // see https://api.drupal.org/api/drupal/includes%21form.inc/function/batch_set/7.x
    batch_set($batch);
  }
}
