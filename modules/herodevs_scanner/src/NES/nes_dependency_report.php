<?php


namespace Drupal\herodevs_scanner\NES;




class NesDependencyReport {
  public function report_header() {
    $last = variable_get('herodevs_scanner_last_report', 0);
    $message = $last ? t('Last checked: @time ago', array('@time' => format_interval(time() - $last))) : t('Last checked: never');
    $checkLink = l(t('Check manually'), 'admin/reports/herodevs/rebuild-report');

    $content = <<<EOF
      <div class="hd--update hd--checked">
        $message
        <span class="hd--check-manually"> $checkLink </span>
      </div>
    EOF;
    // TODO: waht is the early out "data"


    return $content;
  }

  public function report_legend($status_map) {
    $legend_rows = '';
    foreach ($status_map as $key => $status) {
      if (empty($status['legend'])) {
        continue;
      }

      $status_image = theme('image', array('path' => $status['icon'], 'alt' => $status['text'], 'title' => $status['text']));

      $legend_rows .= <<<EOF
        <div class="hd-legend--row">
          <span class="hd-legend--icon">{$status_image}</span>
          <span class="hd-legend--legend">{$status['legend']}</span>
        </div>
      EOF;
    }


    $legend_title = t('Label Definitions');
    $legend_output = <<<EOF
      <div class="hd-legend">
        <div class="hd-legend--title">{$legend_title}</div>
        <hr />
        {$legend_rows}
      </div>
    EOF;
    return $legend_output;
  }
}
