<?php

namespace Drupal\herodevs_scanner\NES;


class NesDependencyReportBatch {


  private $id, $nes;

  // Represents the Updater module's data for a project.
  private $update_data;


  /**
   * Undocumented function
   *
   * @param [type] $id The NES Project FQNS (I think)
   * @param [type] $nes Data from the NES API (pre-chewed by the Oracle)
   * @param [type] $update_data Data from the Updater module
   */
  function __construct($id, $nes, $update_data) {
    $this->id = $id;
    $this->nes = $nes;
    $this->update_data = $update_data;
  }

  /**
   * Produce the report that will be handed to render rows.
   */
  function process() {
    $project = $this->nes;
    $coverage = NULL;

    foreach ($project->reports ?? [] as $r) {
      foreach ($r->entries ?? [] as $e) {
        if (!empty($e->key ?? '') && $e->key === 'nes_coverage') {
          $coverage = $e;
          break;
        }
      }
      if (!is_null($coverage)) {
        break;
      }
    }

    if (empty($coverage)) {
      // TODO: we didn't find anything in NES report for this
    }



    \NES\Log::log('here with project ' . $this->id, $project);
    \NES\Log::log('here with nes data ' . $this->id, $this->nes);
    \NES\Log::log('here with update_data ' . $this->id, $this->update_data);
    // dpm($this->nes);
    // dpm($this->update_data);
    // dpm($project);



    $suggestions = [];

    $modules = $this->update_data['includes'] ?? [];
    $entries = $this->get_entries($project);
    $supported = $this->get_products($coverage);

    // supported, update_available, unavailable, unknown
    //    (or ask_support, but never explicitly set here)
    $status = $this->get_status($coverage, $suggestions);


    // get all the CVEs
    $cves = array_filter($entries, function ($e) {
      return ($e->type ?? 'N/A') == 'CVE';
    });

    // TODO: interpret the reports FOR THIS PROJECT and make consumable for rendering
    return (object) [
      'support' => [
        'products' => $supported,
      ],
      'suggestions' => $suggestions,
      'status' => $status,
      'cves' => $cves,
      'modules' => $modules
    ];
  }

  private function get_products($coverage) {
    $products = [];
    if (!is_null($coverage)) {
      $supported = $coverage->metadata->nes->supported ?? new \stdClass();
      if ($supported->core ?? false) {
        $products[] = 'drupal';
      }
      if ($supported->essentials ?? false) {
        $products[] = 'drupal_7_essentials';
      }
    }
    return $products;
  }

  private function get_status($coverage, &$suggestions) {
    // Choosing 0 instead of null for comparison later.
    $recommended = $coverage->metadata->nes->latest ?? 0;
    $current_version = $this->update_data['existing_version' ?? 'N/A'];

    // If we get to this point, the module is in our API list of supported
    // modules. If we have a recommended version, we can assume it's supported.
    if (empty($recommended) && empty($current_version)) {
      return 'unknown';
    }

    // Check if explicitly not supported.
    $supported = $coverage->metadata->nes->supported ?? new \stdClass();
    if (!$supported->core && !$supported->essentials) {
      return 'unsupported';
    }

    if ($coverage->metadata->nes->excluded ?? false) {
      return 'unsupported';
    }


    // at this point, it's either available OR they're out of date.
    $version_diff = version_compare($current_version, $recommended);
    if ($version_diff < 0) {
      $supported = 'update_available';
      $suggestions[] = (object)[
        'type' => 'remediation',
        'message' => t('Update to latest version (@latest)', ['@latest' => $recommended])
      ];
    }
    // If we don't have a version in the API, can't say that we actively
    // support it.
    elseif ($version_diff > 0) {
      $supported = 'no-issues';
    } else {
      $supported = 'supported';
    }


    \NES\Log::log('here with coverage ' . $this->id, [
      $current_version,
      $recommended,
      $version_diff,
      $supported,
    ]);

    return $supported;
  }

  private function get_entries($project) {
    $entries = array_merge(
      ...array_map(
        function ($r) {
          return $r->entries ?? [];
        },
        $project->reports ?? []
      )
    );
    return $entries;
  }
}
