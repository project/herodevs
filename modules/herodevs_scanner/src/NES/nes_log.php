<?php

namespace NES;


if (!class_exists('\NES\Log')) {

  class Log {
    static function log($message, ...$args) {

      if (defined('NES_LOG_DISABLED')) {
        return;
      }

      $backtrace = debug_backtrace();
      $bt = $backtrace[0];

      // php mode. works pretty well actually
      watchdog('herodevs_scanner', print_r(array_merge(
        [
          'file' => Log::parse_filename($bt['file']) . ':' . $bt['line'],
          'message' => $message,
        ],
        $args
      ), true), []);
    }

    static private function parse_filename($file) {
      return str_replace('/var/www/workbench', './web/sites/all/modules/workbench', $file);
    }
  };
}
