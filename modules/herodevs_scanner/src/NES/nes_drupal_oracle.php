<?php

namespace Drupal\herodevs_scanner\NES;

class NesDrupalOracle {

  protected $mapping;
  protected $reports = [];
  protected $enabled = [];
  public array $modules;

  public function __construct($insightReports) {
    $this->mapping = $this->mapReports($insightReports);
    $this->modules = array_keys($this->mapping);

    module_load_include('inc', 'update', 'update.report');

    $available = array_keys(module_list());

    $module_data = system_rebuild_module_data();
    _update_process_info_list($available, $module_data, 'module', TRUE);

    $this->enabled = array_map(
      function ($e) {
        return \Drupal\herodevs_scanner\NES\NesDrupalOracle::getModuleId($e);
      },
      array_keys(module_list())
    );

    foreach ($this->enabled as $key => $value) {
      # code...
      // dpm($value);
    }
    $available = $this->enabled;

    if ($available) {
      // \NES\Log::log('here with updates', array_keys($available));
    }
  }

  public function getMapping() {
    return $this->mapping;
  }

  public function getEnabled() {
    return $this->enabled;
  }

  public function getModuleDetails(string $moduleId) {
    return array_key_exists($moduleId, $this->mapping)
      ? $this->mapping[$moduleId]
      : null;
  }

  public static function simplifyReport($moduleId, $details) {
    $coverage = null;
    $entries = [];
    foreach ($details->reports as $report) {
      foreach ($report->entries as $entry) {
        // handle coverage (if it hasn't been handled)
        if ($entry->key == 'nes_coverage') {
          if ($coverage === null) {
            $coverage = $entry;
          }
          continue;
        }
        // otherwise just keep the entry?
        $entries[] = $entry;
      }
    }


    $support = null;
    $supportMeta = $coverage->metadata->nes->supported;
    if ($supportMeta->core) {
      $support = 'Core';
    } else if ($supportMeta->essentials) {
      $support = 'Essentials';
    }

    return (object) array(
      'id' => $moduleId,
      'support' => $support,
      'entries' => $entries,
    );
  }

  /**
   * Reduces package reports by package name.
   * Note that a report can address multiple packages.
   */
  private function mapReports($reports) {
    $mapping = [];
    foreach ($reports as $report) {
      // note: future iterations should filter reports without
      // an entry of key "nes_coverage"

      foreach ($report['affectedPackages'] as $pkg) {
        $fqns = $pkg['fqns'];
        if (!array_key_exists($fqns, $mapping)) {
          $mapping[$fqns] = (object)[
            'enabled' => array_key_exists($fqns, $this->enabled),
            'fqns' => $fqns,
            'reports' => []
          ];
        }

        $pkgReports = $mapping[$fqns];
        if (!in_array($report, $pkgReports->reports)) {
          $pkgReports->reports[] = $this->objectify($report);
        }
      }
    }

    return $mapping;
  }


  function objectify($array) {
    // Check if it's an associative array
    if (is_array($array) && array_keys($array) !== range(0, count($array) - 1)) {
      $obj = new \stdClass();
      foreach ($array as $key => $value) {
        $obj->$key = $this->objectify($value);
      }
      return $obj;
    } elseif (is_array($array)) {
      // If it's an indexed array, recurse its elements
      foreach ($array as $key => $value) {
        $array[$key] = $this->objectify($value);
      }
    }
    return $array;
  }


  public static function getModuleId(string $project_name) {
    $herodevs_project_name = 'drupal/' . $project_name;
    // dpm($herodevs_project_name);
    if ($herodevs_project_name == 'drupal/drupal') {
      $herodevs_project_name = 'drupal/core';
    }
    return $herodevs_project_name;
  }
}
