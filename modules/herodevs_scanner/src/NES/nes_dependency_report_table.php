<?php

namespace Drupal\herodevs_scanner\NES;

class NesDependencyTable {

  private $status_map;
  private $report;

  public function __construct() {
    $this->initMaps();
  }

  public function load($force = false) {
    $this->report = [];
    $cache = cache_get('herodevs:full-report');
    if (!empty($cache->data) && !$force) {
      // \NES\Log::log('here with cache', $cache);
      $this->report = $cache->data;
      return false;
    }
    // go get the report?
    return true;
  }

  public function process($force = false) {
    $this->load(false);
    module_load_include('inc', 'update', 'update.report');
    $available = update_get_available(TRUE);
    if ($available) {
      module_load_include('inc', 'update', 'update.compare');
      $compare = update_calculate_project_data($available);
      $parsed = $this->parse_update_data($compare);
      $output = $this->prepare($parsed);
      return $output;
    } else {
      return NULL;
    }
  }

  public function prepare($compare) {
    $rows = $this->prepare_rows($compare);
    $projects_output = $this->process_rows($rows);

    $report = new NesDependencyReport();

    $output = '';
    $output .= $report->report_header();
    $output .= $projects_output . $report->report_legend($this->status_map);
    $output .= $this->system_details($compare);
    return $output;
  }

  public function prepare_rows($data) {

    // \NES\Log::log('here with data', $data);


    $rows = [];
    $this->load();

    $herodevs_projects = $data['drupal_projects'];


    foreach ($herodevs_projects as $id => $project) {
      $fqns = NesDrupalOracle::getModuleId($id) ? NesDrupalOracle::getModuleId($id) : $id;
      $nes = isset($this->report[$fqns]) ? $this->report[$fqns] : NULL;


      $row = $this->unify_reports($id, $fqns, $project, $nes);
      // \NES\Log::log('here with ' . $id, $fqns, $nes);
      $details = $this->generate_tr($row);

      $type = $row->project_type;
      if (!isset($rows[$type])) {
        $rows[$type] = array();
      }

      $rows[$type][] = array(
        'class' => ['hd-project--row', $details->class],
        'data' => array($details->content),
      );
    }

    return $rows;
  }

  function process_rows($rows) {
    // \NES\Log::log('here with rows', $rows);


    $project_types = array(
      'core' => t('Drupal core'),
      'module' => t('Modules'),
      'theme' => t('Themes'),
      'disabled-module' => t('Disabled modules'),
      'disabled-theme' => t('Disabled themes'),
    );

    $header = [];
    $projects_output = '';
    foreach ($project_types as $type_name => $type_label) {
      if (!empty($rows[$type_name])) {
        $rec = $rows[$type_name];
        $table = theme('table', array('header' => $header, 'rows' => $rec, 'attributes' => array('class' => array('hd--update-report'))));
        $projects_output .= <<<EOF
            <h3>{$type_label}</h3>
            {$table}
        EOF;
      }
    }
    return $projects_output;
  }

  // TODO: infer project from $nes
  public function unify_reports($id, $fqns, $project, $nes) {

    if (isset($project['title'])) {
      if (isset($project['link'])) {
        $title = l($project['title'], $project['link']);
      } else {
        $title = check_plain($project['title']);
      }
    } else {
      $title = check_plain($project['name']);
    }

    return (object)[
      'id' => $id,
      'fqns' => $fqns,
      'title' => $title,
      'version' => $project['existing_version'],
      'project_type' => $project['project_type'],
      'report' => $nes
    ];
  }

  public function generate_tr($row) {

    $the_status = isset($row->report->status) ? $row->report->status : 'ask_support';
    if (empty($this->status_map[$the_status])) {
      $the_status = 'ask_support';
    }

    $status = $this->status_map[$the_status];
    $class = 'hd-project--status--' . ($status['class'] ? $status['class'] : '');

    // if ($row->fqns === 'drupal/core') {
    //   \NES\Log::log('status for ' . $row->fqns, $the_status, $status, $row);
    // }



    $project_image = theme('image', array('path' => $status['project_icon'], 'alt' => $status['text'], 'title' => $status['text']));
    $status_image = theme('image', array('path' => $status['icon'], 'alt' => $status['text'], 'title' => $status['text']));

    $expanded_image = theme('image', array('path' => 'misc/arrow-asc.png', 'alt' => 'Dropdown', 'title' => '', 'attributes' => ['class' => 'expanded']));
    $collapsed_image = theme('image', array('path' => 'misc/arrow-desc.png', 'alt' => 'Dropdown', 'title' => '', 'attributes' => ['class' => 'collapsed']));

    // $mods = implode(', ', array_keys($row->report->modules ?? []));
    // \NES\Log::log('report row for for ' . $row->fqns, $row->report);

    $content = <<<EOF
        <div class="hd-project--left">
          <span class="hd-project--status-color">&nbsp;</span>
          <span class="hd-project--status--icon">{$project_image}</span>
          <span class="hd-project--title">{$row->title}</span>
          <span class="hd-project--version">{$row->version}</span>
        </div>
    EOF;


    // dev note: the orphaned </div> here is intentional
    $content .= <<<EOF
          <div class="hd-project--right hd-project--coloured">
            <span class="hd-project--status">
              <span class="hd-project--status--icon">{$status_image}</span>
              <span class="hd-project--status--label">{$status['text']}</span>
              <span class="hd-project--status--icon status">
                {$collapsed_image}
                {$expanded_image}
              </span>
            </span>
            <span class="hd-project--chevron">&nbsp;</span>
          </div>
        </div>
    EOF;

    $suggestions = implode('', array_map(function ($e) {
      return '<li>' . $e->message . '</li>';
    }, isset($row->report->suggestions) ? $row->report->suggestions : []));

    if (!empty($suggestions)) {
      $recommendations = t('Recommendations:');
      $suggestions = "<p><strong>{$recommendations}</strong> <ul>$suggestions</ul><p>";
    }
    // \NES\Log::log('suggestions row for for ' . $row->fqns, $suggestions);
    $content .= <<<EOF
      <div class="hd-project--expanded hd-project--coloured">
        {$status['expanded']}
        {$suggestions}
      </div>
    EOF;
    $content = <<<EOF
      <div class="hd-project--root" data-fqns="{$row->fqns}">
        <div class="clearfix">$content</div>
      </div>
    EOF;
    return (object) [
      'class' => $class,
      'content' => $content,
    ];
  }

  // function herodevs_scanner_calculate_report_data($projects)
  function parse_update_data($projects) {
    global $databases;

    $info = [];
    $info['drupal_core'] = $projects['drupal']['info']['version'];
    $info['webserver'] = $_SERVER['SERVER_SOFTWARE'];
    $info['php_version'] = phpversion();
    $info['database_version'] = \Database::getConnection()->version();
    $info['os_version'] = php_uname();

    $special = [];
    $special['is_multisite'] = file_exists(DRUPAL_ROOT . '/sites/sites.php');
    $special['has_domain_module'] = module_exists("domain");
    $special['count_database_connections'] = count($databases);

    $projects_data = [];
    $drupal_projects_data = [];

    foreach ($projects as $id => $project) {

      $fqns = NesDrupalOracle::getModuleId($id);
      if (!empty($fqns) && array_key_exists($fqns, $this->report)) {
      }
      // \NES\Log::log('Is the report here?', $id, $this->report);
      // \NES\Log::log('MAKE THE REAL STATUS HERE', $id, $fqns);



      $projects_data[$project['name']] = [
        'version' => $project['info']['version'],
        'herodevs_supported' => FALSE,
        'herodevs_roadmap' => TRUE,
      ];

      $drupal_projects_data[$project['name']] = $project;
    }

    $data = [];
    $data['info'] = $info;
    $data['special'] = $special;
    $data['send_counter'] = 0;
    $data['uuid'] = variable_get('herodevs_scanner_uuid');
    $data['user_data'] = [
      'name' => variable_get('herodevs_scanner_settings_organization_name', ''),
      'email' => variable_get('herodevs_scanner_settings_email', ''),
      'notes' => variable_get('herodevs_scanner_settings_notes', ''),
    ];

    $data['drupal_projects'] = $drupal_projects_data;
    $data['projects'] = $projects_data;

    return $data;
  }

  /** Not sure what we should be seeing here... */
  function system_details($data) {
    $sections = [
      'info' => t('General information'),
      'special' => t('Special information'),
      'user_data' => t('User data')
    ];

    $class = ['info'];
    $data_class = ['status-icon', 'status-title', 'status-value'];

    foreach ($sections as $type => $name) {
      $rows[$type] = [];

      foreach ($data[$type] as $key => $value) {
        $rows[$type][] = [
          'class' => $class,
          'data_class' => $data_class,
          'data' => array('', $key, json_encode($value, JSON_PRETTY_PRINT)),
        ];
      }
    }

    $rows['user_data'][] = [
      'class' => $class,
      'data_class' => $data_class,
      'data' => array('', 'UUID', $data['uuid']),
    ];
    $rows['user_data'][] = [
      'class' => $class,
      'data_class' => $data_class,
      'data' => array('', 'Send Counter', $data['send_counter']),
    ];

    $output = '<br />';
    $output .= '<hr />';
    $output .= '<br />';

    foreach ($sections as $type => $name) {
      $output .= "\n<h3>" . $name . "</h3>\n";
      $output .= '<table class="system-status-report">';
      foreach ($rows[$type] as $row) {
        $output .= '<tr class="' . implode(' ', $row['class']) . '">';

        foreach ($row['data'] as $key => $value) {
          $output .= '<td class="' . $row['data_class'][$key] . '">';
          $output .= $value;
          $output .= '</td>';
        }
        $output .= "</tr>";
      }
      $output .= '</table>';
    }
    $output .= '<br />';
    $output .= '<hr />';
    $output .= '<br />';

    return $output;
  }

  public function initMaps() {
    $map = [];
    $map['supported'] = [
      'class' => 'supported',
      'icon' => 'misc/message-24-ok.png',
      'project_icon' => 'misc/message-24-ok.png',
      'text' => t('Supported'),
      'expanded' => t('This module is supported by HeroDevs as part of the HeroDevs Drupal 7 NES Core or Essentials packages.'),
      'legend' => t('Included with and supported in HeroDevs Drupal 7 NES Essentials package.'),
    ];

    $map['no-issues'] = [
      'class' => 'supported',
      'icon' => 'misc/message-24-ok.png',
      'project_icon' => 'misc/message-24-ok.png',
      'text' => t('No Known Issues'),
      'expanded' => t('This Project is Supported and you are using the latest secured version.'),
    ];

    $map['update_available'] = [
      'class' => 'supported',
      'icon' => 'misc/message-24-warning.png',
      'project_icon' => 'misc/message-24-ok.png',
      'text' => t('Update Available'),
      'expanded' => t('This Project is Supported, but you are not using the latest secured version.'),
    ];

    $map['security_advisory'] = [
      'icon' => 'misc/message-24-error.png',
      'project_icon' => 'misc/message-24-error.png',
      'class' => 'security-advisory',
      'text' => t('Security Advisory'),
      'expanded' => t('This Project has a published security advisory. Learn more about the advisory and any recommended steps [here].'),
      'legend' => t('Security Advisory issued, read security advisories here.'),
    ];

    $map['unavailable'] = [
      'icon' => 'misc/message-24-warning.png',
      'project_icon' => 'misc/message-24-warning.png',
      'class' => 'not-supported',
      'text' => t('Not Supported'),
      'expanded' => '',
      'legend' => t('This module is excluded from HeroDevs support packages due to one of the exclusion criteria outlined in HeroDevs Drupal 7 NES SLA.'),
    ];

    $map['ask_support'] = [
      'icon' => 'misc/message-24-info.png',
      'project_icon' => 'misc/message-24-info.png',
      'class' => 'not-supported',
      'text' => t('Contact HeroDevs for more info'),
      'expanded' => t('There is no information available about this Project, which may indicate it is private or closed source. '
        . 'If you feel this is a mistake, please contact Support.'),
      'legend' => t('Contact HeroDevs for more information about this module.'),
    ];

    $this->status_map = $map;
  }
}
