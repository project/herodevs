# Description

This module scans the existing modules on the filesystem to detect those that can be upgraded to the secure HeroDevs version (when one is available).

The list of secure modules available is located at: [TODO]
<HeroDevs URL>

This README includes instructions for:
- Installation
  - Using Update Manager
  - Using Drush
  - Using Composer
- Configuration

# Installation

To install a secure module, choose one of the following methods.

## Using Update Manager

1. Ensure the Update Manager module is enabled in /admin/modules.
1. Visit Administration > Modules > Install new module (/admin/modules/install).
1. Specify the URL for the module from the HeroDevs Scanner report.
1. Click Install.
1. Visit Administration > Modules (/admin/modules), find the module and enable it.

## Using Drush

Ensure Drush is installed. See https://docs.drush.org/en/7.x/install.

1. Download the secure module into to the directory that holds contributed modules. Typically, this is sites/all/modules/contrib.
   Use wget or curl depending on which is installed on your machine.

   https://ftp.drupal.org/files/projects/menu_breadcrumb-7.x-1.6.tar.gz

   ```
   cd <project root>
   wget -P sites/all/modules/contrib <HeroDevs URL>
   ```
   OR [TODO]
   ```
   cd <project root>
   curl -O https://ftp.drupal.org/files/projects/menu_breadcrumb-7.x-1.6.tar.gz
   ```

   Then move the archive file:
   ```
   mv menu_breadcrumb-7.x-1.6.tar.gz sites/all/modules/contrib/
   ```

1. Unzip the archive:
   ```
   gunzip web/sites/all/modules/contrib/menu_breadcrumb-7.x-1.6.tar.gz
   ```

1. Enable the module via the Drupal GUI (/admin/modules) or with Drush:
   ```
   drush en -y herodevs_scanner
   ```

## Using Composer

1. When using Composer, modify the composer.json file to specify the HeroDevs Scanner repository.
   Do this by adding the following to composer.json:
   ```json
         {
            "type": "vcs",
            "url":  "git@github.com:neverendingsupport/d7-herodevs-scanner.git"
         }
   ```
   Typically, this should go just beneath the block that specifies the location of the Drupal packages on Drupal.org, like this:

   ```json
            {
                "type": "composer",
                "url": "https://packages.drupal.org/7"
            },
            {
                "type": "vcs",
                "url":  "git@github.com:neverendingsupport/d7-herodevs-scanner.git"
            }
        ],
        "require": {
   ```

1. Then install the module with the Composer command below. Composer will find the package in the HeroDevs Never Ending Support GitHub repository.

   ```
   composer require drupal/herodevs_scanner
   ```

1. Enable the module via the Drupal GUI (/admin/modules) or with Drush:
   ```
   drush en -y herodevs_scanner
   ```

# Configuration

The configuration page is located on Administration > Configuration > Development > HeroDevs (/admin/config/development/herodevs/settings).

## Configuration items
1. (optional) Provide the organization name and organization email.
1. Specify the access token (if you have one).
