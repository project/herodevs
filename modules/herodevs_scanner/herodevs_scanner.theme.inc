<?php
if (!defined('HERODEVS_STATUS_SUPPORTED')) {
  define('HERODEVS_STATUS_ROADMAP', 1);
  define('HERODEVS_STATUS_UPDATE', 4);
  define('HERODEVS_STATUS_UNSUPPORTED', 3);
  define('HERODEVS_STATUS_SUPPORTED', 5);
}
/**
 * Theme project status report.
 *
 * @ingroup themeable
 */
function theme_herodevs_scanner_report($variables) {
  $data = $variables['data'];
  $output = '';
  if (!is_array($data)) {
    $output .= '<p>' . $data . '</p>';
    return $output;
  }

  $table = new \Drupal\herodevs_scanner\NES\NesDependencyTable();
  $output = $table->process();

  if (is_null($output)) {
    $output = '<p> Unexpected <em>null</em> output from Dependency Table. Please contact support.';
  }

  return $output;
}

/**
 * Theme function for the message bar.
 */
function theme_herodevs_scanner_message_bar($variables) {
  $settings = $variables['info'];
  $output = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        drupal_html_class('herodevs-scanner-message-bar'),
        'position-' . $settings['herodevs_scanner_settings_position'],
        'fixed-' . ($settings['herodevs_scanner_settings_fixed'] ? 'yes' : 'no'),
      ),
      'id' => 'herodevs-scanner-message-bar',
      'style' => 'background-color: ' . $settings['herodevs_scanner_settings_color'] . '; color: ' . $settings['herodevs_scanner_settings_text_color'] . '; text-align: center;',
    ),
    'children' => array(
      'text' => array(
        '#markup' => '<p>' . HERODEVS_SCANNER_SUPPORT_MSG . '</p>',
      ),
    ),
  );
  return drupal_render($output);
}
